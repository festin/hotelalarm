package pro.bbgroup.hotel.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Саша on 10.12.2014.
 */
public class ListConfirmingDocTypes {
    ObservableList<KeyValuePair> docTypes = FXCollections.observableArrayList();
    public ObservableList<KeyValuePair> getDocTypes() {
        return docTypes;
    }

    public ListConfirmingDocTypes() {
        docTypes.add(new KeyValuePair("visa", "Виза"));
        docTypes.add(new KeyValuePair("res_permit", "Вид на жительство"));
        docTypes.add(new KeyValuePair("temp_res_permit", "Разрешение на временное проживание"));
    }
}
