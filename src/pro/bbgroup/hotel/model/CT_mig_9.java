package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "ns9:case")
@XmlType(propOrder={"notificationReceived", "stayPeriod",
        "personDataDocument",
        // "stayPlace",
        // "docResidence",
        "sono", "notificationNumber", "noticeFrom",
        "firstArrival", "stateProgramMember",
        // "host",
        "entrancePurpose",
        "migrationCard",
        "profession"})

public class CT_mig_9 extends Case {
    private StringProperty notificationReceived;
    private StayPeriod stayPeriod;
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration")
    public PersonalDataDocument personDataDocument;
    private StringProperty personalDocumentEntered;
    private StringProperty stayRusAddressObj;
    private StringProperty stayRusAddressHousing1;
    private StringProperty confirmingDocType;
    private StringProperty confirmingDocUid;
    private StringProperty confirmingDocEntered;
    private StringProperty sono;
    private StringProperty notificationNumber;
    private StringProperty noticeFrom;
    private StringProperty firstArrival;
    private StringProperty stateProgramMember;
    private Organization hostOrg;
    private Person hostPerson;
    private StringProperty hostPersonAddrObj;
    private StringProperty hostPersonAddrHousing1;
    private StringProperty hostPersonTel;
    private Dict entrancePurpose;
    private MigrationCard migrationCard;
    private Dict profession;

    public StringProperty lastNameProperty() {
        return personDataDocument.person.lastNameProperty();
    }
    public StringProperty firstNameProperty() {
        return personDataDocument.person.firstNameProperty();
    }
    public void setHostOrg(Organization hostOrg) {
        this.hostOrg = hostOrg;
    }
    public void setHostPerson(Person hostPerson) {
        this.hostPerson = hostPerson;
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration")
    public StayPeriod getStayPeriod() {
        return stayPeriod;
    }

    public void setStayPeriod(StayPeriod stayPeriod) {
        this.stayPeriod = stayPeriod;
    }

    public CT_mig_9() {
        this.setUid("Random_uid_4");
        this.notificationReceived = new SimpleStringProperty("0000-00-00");
        this.confirmingDocType = new SimpleStringProperty("---");
        this.stayPeriod = new StayPeriod("0000-00-00", "9999-12-12");
        this.personDataDocument = new PersonalDataDocument();
        this.personalDocumentEntered = new SimpleStringProperty("---");
        this.stayRusAddressObj = new SimpleStringProperty("---");
        this.stayRusAddressHousing1 = new SimpleStringProperty("---");
        this.confirmingDocType = new SimpleStringProperty("---");
        this.confirmingDocUid = new SimpleStringProperty("---");
        this.confirmingDocEntered = new SimpleStringProperty("---");
        this.sono = new SimpleStringProperty("---");
        this.notificationNumber = new SimpleStringProperty("---");
        this.noticeFrom = new SimpleStringProperty("---");
        this.firstArrival = new SimpleStringProperty("---");
        this.stateProgramMember = new SimpleStringProperty("---");
        this.hostOrg = new Organization();
        this.hostPerson = new Person();
        this.hostPersonAddrObj = new SimpleStringProperty("---");
        this.hostPersonAddrHousing1 = new SimpleStringProperty("---");
        this.hostPersonTel = new SimpleStringProperty("---");
        this.entrancePurpose = new Dict("VisitPurpose", "139348", "");
        this.migrationCard = new MigrationCard();
        this.profession = new Dict("Profession", "128114", "");

    }


    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration")
    public String getNotificationReceived() {
        return notificationReceived.get();
    }

    public StringProperty notificationReceivedProperty() {
        return notificationReceived;
    }

    public void setNotificationReceived(String notificationReceived) {
        this.notificationReceived.set(notificationReceived);
    }

    @XmlTransient
    public String getPersonalDocumentEntered() {
        return personalDocumentEntered.get();
    }

    public StringProperty personalDocumentEnteredProperty() {
        return personalDocumentEntered;
    }

    public void setPersonalDocumentEntered(String personalDocumentEntered) {
        this.personalDocumentEntered.set(personalDocumentEntered);
    }
    @XmlTransient
    public String getStayRusAddressObj() {
        return stayRusAddressObj.get();
    }

    public StringProperty stayRusAddressObjProperty() {
        return stayRusAddressObj;
    }

    public void setStayRusAddressObj(String stayRusAddressObj) {
        this.stayRusAddressObj.set(stayRusAddressObj);
    }
    @XmlTransient
    public String getStayRusHousing1() {
        return stayRusAddressHousing1.get();
    }

    public StringProperty stayRusHousing1Property() {
        return stayRusAddressHousing1;
    }

    public void setStayRusHousing1(String stayRusHousing1) {
        this.stayRusAddressHousing1.set(stayRusHousing1);
    }
    @XmlTransient
    public String getConfirmingDocType() {
        return confirmingDocType.get();
    }

    public StringProperty confirmingDocTypeProperty() {
        return confirmingDocType;
    }

    public void setConfirmingDocType(String confirmingDocType) {
        this.confirmingDocType.set(confirmingDocType);
    }
    @XmlTransient
    public String getConfirmingDocUid() {
        return confirmingDocUid.get();
    }

    public StringProperty confirmingDocUidProperty() {
        return confirmingDocUid;
    }

    public void setConfirmingDocUid(String confirmingDocUid) {
        this.confirmingDocUid.set(confirmingDocUid);
    }
    @XmlTransient
    public String getConfirmingDocEntered() {
        return confirmingDocEntered.get();
    }

    public StringProperty confirmingDocEnteredProperty() {
        return confirmingDocEntered;
    }

    public void setConfirmingDocEntered(String confirmingDocEntered) {
        this.confirmingDocEntered.set(confirmingDocEntered);
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration")
    public String getSono() {
        return sono.get();
    }

    public StringProperty sonoProperty() {
        return sono;
    }

    public void setSono(String sono) {
        this.sono.set(sono);
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public String getNotificationNumber() {
        return notificationNumber.get();
    }

    public StringProperty notificationNumberProperty() {
        return notificationNumber;
    }

    public void setNotificationNumber(String notificationNumber) {
        this.notificationNumber.set(notificationNumber);
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public String getNoticeFrom() {
        return noticeFrom.get();
    }

    public StringProperty noticeFromProperty() {
        return noticeFrom;
    }

    public void setNoticeFrom(String noticeFrom) {
        this.noticeFrom.set(noticeFrom);
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public String getFirstArrival() {
        return firstArrival.get();
    }

    public StringProperty firstArrivalProperty() {
        return firstArrival;
    }

    public void setFirstArrival(String firstArrival) {
        this.firstArrival.set(firstArrival);
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public String getStateProgramMember() {
        return stateProgramMember.get();
    }

    public StringProperty stateProgramMemberProperty() {
        return stateProgramMember;
    }

    public void setStateProgramMember(String stateProgramMember) {
        this.stateProgramMember.set(stateProgramMember);
    }
    @XmlTransient
    public Organization getHostOrg() {
        return hostOrg;
    }
    @XmlTransient
    public Person getHostPerson() {
        return hostPerson;
    }
    @XmlTransient
    public String getHostPersonAddrObj() {
        return hostPersonAddrObj.get();
    }

    public StringProperty hostPersonAddrObjProperty() {
        return hostPersonAddrObj;
    }

    public void setHostPersonAddrObj(String hostPersonAddrObj) {
        this.hostPersonAddrObj.set(hostPersonAddrObj);
    }
    @XmlTransient
    public String getHostPersonAddrHousing1() {
        return hostPersonAddrHousing1.get();
    }

    public StringProperty hostPersonAddrHousing1Property() {
        return hostPersonAddrHousing1;
    }

    public void setHostPersonAddrHousing1(String hostPersonAddrHousing1) {
        this.hostPersonAddrHousing1.set(hostPersonAddrHousing1);
    }
    @XmlTransient
    public String getHostPersonTel() {
        return hostPersonTel.get();
    }

    public StringProperty hostPersonTelProperty() {
        return hostPersonTel;
    }

    public void setHostPersonTel(String hostPersonTel) {
        this.hostPersonTel.set(hostPersonTel);
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public Dict getEntrancePurpose() {
        return entrancePurpose;
    }

    public void setEntrancePurpose(Dict entrancePurpose) {
        this.entrancePurpose = entrancePurpose;
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public MigrationCard getMigrationCard() {
        return migrationCard;
    }
    @XmlElement(namespace = "http://umms.fms.gov.ru/replication/migration/staying")
    public Dict getProfession() {
        return profession;
    }
    public void setProfession(Dict profession) {
        this.profession = profession;
    }

}
