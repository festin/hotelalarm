package pro.bbgroup.hotel.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Саша on 10.12.2014.
 */
public class ListVisitPurposes {
    ObservableList<KeyValuePair> visitPurposes = FXCollections.observableArrayList();

    public ObservableList<KeyValuePair> getVisitPurposes() {
        return visitPurposes;
    }
    public ListVisitPurposes() {
        visitPurposes.add(new KeyValuePair("139344", "Служебная"));
        visitPurposes.add(new KeyValuePair("139345", "Туризм"));

        visitPurposes.add(new KeyValuePair("139346", "Деловая"));
        visitPurposes.add(new KeyValuePair("139347", "Учеба"));
        visitPurposes.add(new KeyValuePair("139348", "Работа"));
        visitPurposes.add(new KeyValuePair("139349", "Частная"));
        visitPurposes.add(new KeyValuePair("139350", "Транзит"));
        visitPurposes.add(new KeyValuePair("139351", "Гуманитарная"));
        visitPurposes.add(new KeyValuePair("139352", "Получение убежища"));
        visitPurposes.add(new KeyValuePair("139353", "Другая"));

    }

}



