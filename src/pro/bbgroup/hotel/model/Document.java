package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Document {
    public StringProperty uid;
    public Dict type;
    public StringProperty series;
    public StringProperty number;
    public StringProperty authority;
    public Dict authorityOrgan;
    public StringProperty issued;
    public StringProperty valid_from;
    public StringProperty valid_to;
    public Dict status;

    public Document() {
        this.uid = new SimpleStringProperty();
        this.type = new Dict("DocumentType", "---", "");
        this.series = new SimpleStringProperty();
        this.number = new SimpleStringProperty();
        this.authority = new SimpleStringProperty();
        this.authorityOrgan = new Dict("officialOrgan", "---", "");
        this.issued = new SimpleStringProperty();
        this.valid_from = new SimpleStringProperty();
        this.valid_to = new SimpleStringProperty();
        this.status = new Dict("DocumentStatus", "---", "");
    }
    public Document(String uid, String documentType, String series,
                    String number, String authority, String issued, String valid_from, String valid_to, String status) {

        this.uid = new SimpleStringProperty(uid);
        this.type = new Dict("DocumentType", "---", "");
        this.series = new SimpleStringProperty(series);
        this.number = new SimpleStringProperty(number);
        this.authority = new SimpleStringProperty(authority);
        this.issued = new SimpleStringProperty(issued);
        this.valid_from = new SimpleStringProperty(valid_from);
        this.valid_to = new SimpleStringProperty(valid_to);
        this.status = new Dict("DocumentStatus", "---", "");
    }

    public String getUid() {
        return uid.get();
    }

    public StringProperty uidProperty() {
        return uid;
    }

    public Dict getDocumentType() {        return type;    }

    public Dict documentTypeProperty() {
        return type;
    }

    public String getSeries() {
        return series.get();
    }

    public StringProperty seriesProperty() {
        return series;
    }

    public String getNumber() {
        return number.get();
    }

    public StringProperty numberProperty() {
        return number;
    }

    public String getAuthority() {
        return authority.get();
    }

    public StringProperty authorityProperty() {
        return authority;
    }

    public String getIssued() {
        return issued.get();
    }

    public StringProperty issuedProperty() {
        return issued;
    }

    public String getValid_from() {
        return valid_from.get();
    }

    public StringProperty valid_fromProperty() {
        return valid_from;
    }

    public String getValid_to() {
        return valid_to.get();
    }

    public StringProperty valid_toProperty() {
        return valid_to;
    }

    public Dict getStatus() {
        return status;
    }

}

