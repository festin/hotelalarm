package pro.bbgroup.hotel.model;

import com.sun.xml.internal.bind.marshaller.NamespacePrefixMapper;
//import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class MyNamespaceMapper extends NamespacePrefixMapper {

    private static final String FOO_PREFIX = ""; // DEFAULT NAMESPACE
    private static final String FOO_URI = "http://umms.fms.gov.ru/replication/core";
    private static final String NS2_PREFIX = "ns2";
    private static final String NS2_URI = "http://umms.fms.gov.ru/replication/core/correction";
    private static final String NS3_PREFIX = "ns3";
    private static final String NS3_URI = "http://umms.fms.gov.ru/replication/foreign-citizen-core";
    private static final String NS4_PREFIX = "ns4";
    private static final String NS4_URI = "http://umms.fms.gov.ru/replication/migration";
    private static final String NS5_PREFIX = "ns5";
    private static final String NS5_URI = "http://umms.fms.gov.ru/replication/hotel";
    private static final String NS6_PREFIX = "ns6";
    private static final String NS6_URI = "http://umms.fms.gov.ru/replication/migration/staying/case-edit";

    private static final String NS7_PREFIX = "ns7";
    private static final String NS7_URI = "http://umms.fms.gov.ru/replication/hotel/form5";
    private static final String NS8_PREFIX = "ns8";
    private static final String NS8_URI = "http://umms.fms.gov.ru/replication/migration/staying/unreg";
    private static final String NS9_PREFIX = "ns9";
    private static final String NS9_URI = "http://umms.fms.gov.ru/replication/migration/staying";

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if(FOO_URI.equals(namespaceUri)) {
            return FOO_PREFIX;
        } else if(NS2_URI.equals(namespaceUri)) {
            return NS2_PREFIX;
        } else if(NS3_URI.equals(namespaceUri)) {
            return NS3_PREFIX;
        } else if(NS4_URI.equals(namespaceUri)) {
            return NS4_PREFIX;
        } else if(NS5_URI.equals(namespaceUri)) {
            return NS5_PREFIX;
        } else if(NS6_URI.equals(namespaceUri)) {
            return NS6_PREFIX;
        } else if(NS7_URI.equals(namespaceUri)) {
            return NS7_PREFIX;
        } else if(NS8_URI.equals(namespaceUri)) {
            return NS8_PREFIX;
        } else if(NS9_URI.equals(namespaceUri)) {
            return NS9_PREFIX;
        }
        return suggestion;
    }

    @Override
    public String[] getPreDeclaredNamespaceUris() {
        return new String[] { FOO_URI, NS2_URI, NS3_URI, NS4_URI, NS5_URI, NS6_URI, NS7_URI, NS8_URI, NS9_URI };
    }

}