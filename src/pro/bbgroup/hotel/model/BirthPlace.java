package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Саша on 11.12.2014.
 */
public class BirthPlace {
    private Dict country;
    private StringProperty place;
    private StringProperty place2;
    private StringProperty place3;
    private StringProperty place4;

    public BirthPlace() {
        this.country = new Dict("Country", "---", "");
        this.place = new SimpleStringProperty("---");
        this.place2 = new SimpleStringProperty("---");;
        this.place3 = new SimpleStringProperty("---");;
        this.place4 = new SimpleStringProperty("---");;
    }

    public Dict getCountry() {
        return country;
    }

    public void setCountry(Dict country) {
        this.country = country;
    }

    public String getPlace() {
        return place.get();
    }

    public StringProperty placeProperty() {
        return place;
    }

    public void setPlace(String place) {
        this.place.set(place);
    }

    public String getPlace2() {
        return place2.get();
    }

    public StringProperty place2Property() {
        return place2;
    }

    public void setPlace2(String place2) {
        this.place2.set(place2);
    }

    public String getPlace3() {
        return place3.get();
    }

    public StringProperty place3Property() {
        return place3;
    }

    public void setPlace3(String place3) {
        this.place3.set(place3);
    }

    public String getPlace4() {
        return place4.get();
    }

    public StringProperty place4Property() {
        return place4;
    }

    public void setPlace4(String place4) {
        this.place4.set(place4);
    }
}
