package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Саша on 10.12.2014.
 */
public class StayPeriod {
    public String getDateFrom() {
        return dateFrom.get();
    }

    public StringProperty dateFromProperty() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom.set(dateFrom);
    }

    public String getDateTo() {
        return dateTo.get();
    }

    public StringProperty dateToProperty() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo.set(dateTo);
    }

    private final StringProperty dateFrom;
    private final StringProperty dateTo;


    public StayPeriod(String dateFrom, String dateTo) {
        this.dateFrom = new SimpleStringProperty(dateFrom);
        this.dateTo = new SimpleStringProperty(dateTo);
    }
}

