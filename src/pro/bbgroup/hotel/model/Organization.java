package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Саша on 09.12.2014.
 CREATE TABLE Organizations
 (id integer primary key autoincrement,
 uid text,
 inn text,
 name text,
 address_object text,
 address_housing1 text,
 address_housing2 text,
 address_housing3 text,
 address_housing4 text,
 address_housing5 text,
 address_housing6 text,
 address_housing7 text,
 address_housing8 text
 );
 */
public class Organization {
    private final StringProperty uid;
    private final StringProperty inn;
    private final StringProperty name;
    private final StringProperty address_object;
    private final StringProperty address_housing1;
    /*
    private final StringProperty address_housing2;
    private final StringProperty address_housing3;
    private final StringProperty address_housing4;
    private final StringProperty address_housing5;
    private final StringProperty address_housing6;
    private final StringProperty address_housing7;
    private final StringProperty address_housing8;
    */

    public Organization() {
        this.uid = new SimpleStringProperty("---");
        this.inn = new SimpleStringProperty("---");
        this.name = new SimpleStringProperty("---");
        this.address_object = new SimpleStringProperty("---");
        this.address_housing1 = new SimpleStringProperty("---");
    }
    public Organization(String uid, String inn, String name,
                        String address_object, String address_housing1 /*, String address_housing2,
                         String address_housing3, String address_housing4, String address_housing5,
                         String address_housing6, String address_housing7, String address_housing8*/) {
        this.uid = new SimpleStringProperty(uid);
        this.inn = new SimpleStringProperty(inn);
        this.name = new SimpleStringProperty(name);
        this.address_object = new SimpleStringProperty(address_object);
        this.address_housing1 = new SimpleStringProperty(address_housing1);
        /*
        this.address_housing2 = new SimpleStringProperty(address_housing2);
        this.address_housing3 = new SimpleStringProperty(address_housing3);
        this.address_housing4 = new SimpleStringProperty(address_housing4);
        this.address_housing5 = new SimpleStringProperty(address_housing5);
        this.address_housing6 = new SimpleStringProperty(address_housing6);
        this.address_housing7 = new SimpleStringProperty(address_housing7);
        this.address_housing8 = new SimpleStringProperty(address_housing8);
        */
    }
}
