package pro.bbgroup.hotel.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Helper class to wrap a list of persons. This is used for saving the
 * list of persons to XML.
 *
 * @author Marco Jakob
 */
@XmlRootElement(name = "wrap")
public class CaseListWrapper {

    private List<CT_mig_9> cases;

    @XmlElement(name = "case")
    public List<CT_mig_9> getCases() {
        return cases;
    }

    public void setCases(List<CT_mig_9> cases) {
        this.cases = cases;
    }
}
