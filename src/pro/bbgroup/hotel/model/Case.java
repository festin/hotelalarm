package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Саша on 09.12.2014.
 */
//@XmlRootElement(name = "case")
//@XmlType(namespace="http://umms.fms.gov.ru/replication/core")
@XmlType(propOrder={"uid", "requestId", "supplierInfo", "subdivision", "employee", "date"})
public class Case {
    public String getUid() {
        return uid.get();
    }

    public StringProperty uidProperty() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid.set(uid);
    }

    public String getRequestId() {
        return requestId.get();
    }

    public StringProperty requestIdProperty() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId.set(requestId);
    }

    public String getSupplierInfo() {
        return supplierInfo.get();
    }

    public StringProperty supplierInfoProperty() {
        return supplierInfo;
    }

    public void setSupplierInfo(String supplierInfo) {
        this.supplierInfo.set(supplierInfo);
    }

    public String getSubdivision() {
        return subdivision.get();
    }

    public StringProperty subdivisionProperty() {
        return subdivision;
    }

    public void setSubdivision(String subdivision) {
        this.subdivision.set(subdivision);
    }

    public String getDate() {
        return date.get();
    }

    public StringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
    }

    public String getEmployee() {
        return employee.get();
    }

    public StringProperty employeeProperty() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee.set(employee);
    }

    private final StringProperty uid;
    private final StringProperty requestId;
    private final StringProperty supplierInfo;
    private final StringProperty subdivision;
    private final StringProperty date;
    private final StringProperty employee;

    public Case() {
        this.uid = new SimpleStringProperty("Random_uid_4");
        this.requestId = new SimpleStringProperty("RequestId_construct");
        this.supplierInfo = new SimpleStringProperty("SupplierInfo_constructor");
        this.subdivision = new SimpleStringProperty("Subdivision_constructor");
        this.date = new SimpleStringProperty("0000-00-00T00:00:00+04:00");
        this.employee = new SimpleStringProperty("Employee_constructor");
    }



}
