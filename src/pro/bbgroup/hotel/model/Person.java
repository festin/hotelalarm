package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(propOrder={"uid", "personId", "lastName", "lastNameLat",
    "firstName", "firstNameLat", "middleName", "middleNameLat", "gender", "birthDate",
    "citizenship", "birthPlace"})
public class Person {
    public StringProperty uid;
    private StringProperty firstName;
    private StringProperty firstNameLat;
    private StringProperty lastName;
    private StringProperty lastNameLat;
    private StringProperty middleName;
    private StringProperty middleNameLat;
    public StringProperty personId;
    public Dict gender;
    private StringProperty birthDate;
    public Dict citizenship; //Гражданство, подданство (код ОКСМ)
    public BirthPlace birthPlace;

    // @XmlJavaTypeAdapter(AdapterLocalDate.class)
    public String getBirthDate() {
        return birthDate.get();
    }
    public void setBirthDate(String str) {
        this.birthDate.set(str);
    }
    public StringProperty BirthDateProperty() {
        return birthDate;
    }
    public String getFirstName() {
        return firstName.get();
    }
    public void setFirstName(String str) {
        this.firstName.set(str);
    }
    public StringProperty firstNameProperty() {
        return firstName;
    }
    public String getLastName() {
        return lastName.get();
    }
    public void setLastName(String str) {
        this.lastName.set(str);
    }
    public StringProperty lastNameProperty() {
        return lastName;
    }
    public String getLastNameLat() {
        return lastNameLat.get();
    }
    public void setLastNameLat(String str) {
        this.lastNameLat.set(str);
    }
    public StringProperty lastNameLatProperty() {
        return lastNameLat;
    }
    public String getFirstNameLat() {
        return firstNameLat.get();
    }
    public void setFirstNameLat(String firstNameLat) {
        this.firstNameLat.set(firstNameLat);
    }
    public StringProperty firstNameLatProperty() {       return firstNameLat;    }
    public String getMiddleNameLat() {
        return middleNameLat.get();
    }
    public void setMiddleNameLat(String middleNameLat) {
        this.middleNameLat.set(middleNameLat);
    }
    public StringProperty middleNameLatProperty() {       return middleNameLat;    }
    public String getMiddleName() {
        return middleName.get();
    }
    public void setMiddleName(String middleName) {
        this.middleName.set(middleName);
    }
    public StringProperty middleNameProperty() {       return middleNameLat;    }

    public Person() {
        uid = new SimpleStringProperty("random_uid_4");
        personId = uid;
        firstName = new SimpleStringProperty("---");
        firstNameLat = new SimpleStringProperty("FirstNameLat");
        lastName = new SimpleStringProperty("---");
        lastNameLat = new SimpleStringProperty("LastNameLat");
        middleName = new SimpleStringProperty("---");
        middleNameLat = new SimpleStringProperty("---");
        citizenship = new Dict("Citizenship", "AUS", "Austria");
        birthDate = new SimpleStringProperty("---");
        gender = new Dict("Gender", "М", "Мужской");
        birthPlace = new BirthPlace();
    }

   public Person(String uid, String firstName, String lastName) {
        this.uid = new SimpleStringProperty(uid);
       this.personId = this.uid;
        this.firstName = new SimpleStringProperty(firstName);
        this.firstNameLat = new SimpleStringProperty("FirstNameLat");
        this.lastName = new SimpleStringProperty(lastName);
        this.lastNameLat = new SimpleStringProperty("LastNameLat");
        this.middleName = new SimpleStringProperty("---");
        this.middleNameLat = new SimpleStringProperty("---");
        this.citizenship = new Dict("Citizenship", "AUS", "Austria");
        // Some initial dummy data, just for convenient testing.
        this.gender = new Dict("Gender", "М", "Мужской");
        this.birthDate = new SimpleStringProperty("0000-00-00");
        this.birthPlace = new BirthPlace();
   }


    /*
    public String getLastNameLat() {
        return lastNameLat.get();
    }
    public void setLastNameLat(String lastNameLat) {
        this.lastNameLat.set(lastNameLat);
    }
    public StringProperty lastNameLatProperty() {
        return lastNameLat;
    }

    public String getFirstNameLat() {
        return firstNameLat.get();
    }
    public void setFirstNameLat(String firstNameLat) {
        this.firstNameLat.set(firstNameLat);
    }
    public StringProperty firstNameLatProperty() {
        return firstNameLat;
    }
*/

}
