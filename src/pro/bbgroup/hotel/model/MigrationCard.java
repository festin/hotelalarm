package pro.bbgroup.hotel.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Саша on 09.12.2014.
 CREATE TABLE MigrationCards
 (uid text,
 series text,
 number text,
 stay_from text,
 stay_to text,
 entrance_date date,
 entrance_checkpoint text,
 visit_purpose text
 );
 */
public class MigrationCard {
    private final StringProperty uid;
    private final StringProperty series;
    private final StringProperty number;
    private final StringProperty stay_from;
    private final StringProperty stay_to;
    private final StringProperty entrance_date;
    private final StringProperty entrance_checkpoint;
    private final StringProperty visit_purpose;

    public String getEntrance_checkpoint() {
        return entrance_checkpoint.get();
    }
    public StringProperty entrance_checkpointProperty() {
        return entrance_checkpoint;
    }
    public void setEntrance_checkpoint(String entrance_checkpoint) {
        this.entrance_checkpoint.set(entrance_checkpoint);
    }
    public String getEntrance_date() {
        return entrance_date.get();
    }
    public StringProperty entrance_dateProperty() {
        return entrance_date;
    }
    public void setEntrance_date(String entrance_date) {
        this.entrance_date.set(entrance_date);
    }
    public String getNumber() {
        return number.get();
    }
    public StringProperty numberProperty() {
        return number;
    }
    public void setNumber(String number) {
        this.number.set(number);
    }
    public String getSeries() {
        return series.get();
    }
    public StringProperty seriesProperty() {
        return series;
    }
    public void setSeries(String series) {
        this.series.set(series);
    }
    public String getStay_from() {
        return stay_from.get();
    }
    public StringProperty stay_fromProperty() {
        return stay_from;
    }
    public void setStay_from(String stay_from) {
        this.stay_from.set(stay_from);
    }
    public String getStay_to() {
        return stay_to.get();
    }
    public StringProperty stay_toProperty() {
        return stay_to;
    }
    public void setStay_to(String stay_to) {
        this.stay_to.set(stay_to);
    }
    public String getUid() {
        return uid.get();
    }
    public StringProperty uidProperty() {
        return uid;
    }
    public void setUid(String uid) {
        this.uid.set(uid);
    }
    public String getVisit_purpose() {
        return visit_purpose.get();
    }
    public StringProperty visit_purposeProperty() {
        return visit_purpose;
    }
    public void setVisit_purpose(String visit_purpose) {
        this.visit_purpose.set(visit_purpose);
    }

    public MigrationCard(String id, String uid, String series,String number, String stay_from, String stay_to,
                         String entrance_date, String entrance_checkpoint, String visit_purpose) {
        this.uid = new SimpleStringProperty(uid);
        this.series = new SimpleStringProperty(series);
        this.number = new SimpleStringProperty(number);
        this.stay_from = new SimpleStringProperty(stay_from);
        this.stay_to = new SimpleStringProperty(stay_to);
        this.entrance_date = new SimpleStringProperty(entrance_date);
        this.entrance_checkpoint = new SimpleStringProperty(entrance_checkpoint);
        this.visit_purpose = new SimpleStringProperty(visit_purpose);
    }
    public MigrationCard(){
        this.uid = new SimpleStringProperty();
        this.series = new SimpleStringProperty();
        this.number = new SimpleStringProperty();
        this.stay_from = new SimpleStringProperty();
        this.stay_to = new SimpleStringProperty();
        this.entrance_date = new SimpleStringProperty();
        this.entrance_checkpoint = new SimpleStringProperty();
        this.visit_purpose = new SimpleStringProperty();
    }
}
