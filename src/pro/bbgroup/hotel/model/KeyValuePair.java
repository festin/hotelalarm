package pro.bbgroup.hotel.model;

/**
 * Created by festin on 09.12.14.
 */
public class KeyValuePair {
    private final String key;
    private final String value;
    public KeyValuePair(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey()   {    return key;    }
    public String toString() {    return value;  }
}
