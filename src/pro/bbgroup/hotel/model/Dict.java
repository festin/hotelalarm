package pro.bbgroup.hotel.model;

/**
 * Created by Саша on 10.12.2014.
 */

import javafx.beans.property.Property;

import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"type", "element", "value"})
public class Dict {
    private String type;
    private String element;
    private String value;

    public Dict(String type, String element, String value) {
        this.type = type;
        this.element = element;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }






}
