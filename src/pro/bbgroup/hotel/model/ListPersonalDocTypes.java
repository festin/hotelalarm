package pro.bbgroup.hotel.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Саша on 10.12.2014.
 */
public class ListPersonalDocTypes {
    ObservableList<KeyValuePair> docTypes = FXCollections.observableArrayList();
    public ObservableList<KeyValuePair> getDocTypes() {
        return docTypes;
    }

    public ListPersonalDocTypes() {
        docTypes.add(new KeyValuePair("103008","Паспорт гражданина Российской Федерации"));
        docTypes.add(new KeyValuePair("102973","Аттестат"));
        docTypes.add(new KeyValuePair("102990","Военный билет солдата (матроса, сержанта, старшины)"));
    }
}
