package pro.bbgroup.hotel.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Саша on 10.12.2014.
 */
public class ListGenders {
    ObservableList<KeyValuePair> genders = FXCollections.observableArrayList();

    public ObservableList<KeyValuePair> getGenders() {
        return genders;
    }
    public ListGenders() {
        genders.add(new KeyValuePair("M", "М"));
        genders.add(new KeyValuePair("F", "Ж"));
    }

}
