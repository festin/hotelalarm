package pro.bbgroup.hotel.model;

import javax.swing.text.Document;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Саша on 11.12.2014.
 */
public class PersonalDataDocument {
    public Person person;
    public pro.bbgroup.hotel.model.Document document;
    public Boolean entered;

    public PersonalDataDocument() {
        this.person = new Person();
        this.document = new pro.bbgroup.hotel.model.Document();
        this.entered = false;
    }

}
