package pro.bbgroup.hotel;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import pro.bbgroup.hotel.model.*;
import pro.bbgroup.hotel.view.CaseEditDialogController;
import pro.bbgroup.hotel.view.CaseOverviewController;
import pro.bbgroup.hotel.view.RootLayoutController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    private ObservableList<Case> caseData = FXCollections.observableArrayList();
    private ObservableList<CT_mig_9> case9 = FXCollections.observableArrayList();
    private Organization orgHost;

    public Person getPersonHost() {
        return personHost;
    }

    public Organization getOrgHost() {
        return orgHost;
    }

    private Person personHost;

    public MainApp() {

    }

    public ObservableList<Case> getCaseData() {
        return caseData;
    }

    @Override
    public void start(Stage primaryStage) {
        // TODO заполнить информацию о принимающей стороне
        // она должна быть hardcoded.
        System.out.println("Load host data: " + loadHostData());

        // Загрузка имеющихся данных
        System.out.println("Load saved data: " + loadSavedData());


        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("AddressApp");
        this.primaryStage.getIcons().add(new Image("file:resources/images/icon.png"));
        initRootLayout();

        showPersonOverview();
    }

    /**
     * Initializes the root layout and tries to load the last opened
     * person file.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showPersonOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/CaseOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            CaseOverviewController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public boolean showCaseEditDialog(CT_mig_9 tempCase) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/CaseEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Case");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            CaseEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setCase(tempCase);
            controller.setMainApp(this);
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
    public File getPersonFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        String filePath = prefs.get("filePath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }


    public void setPersonFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        if (file != null) {
            prefs.put("filePath", file.getPath());

            // Update the stage title.
            primaryStage.setTitle("AddressApp - " + file.getName());
        } else {
            prefs.remove("filePath");

            // Update the stage title.
            primaryStage.setTitle("AddressApp");
        }
    }
*/
    public void loadPersonDataFromFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(CaseListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            // Reading XML from the file and unmarshalling.
            CaseListWrapper wrapper = (CaseListWrapper) um.unmarshal(file);

            case9.clear();
            case9.addAll(wrapper.getCases());

            // Save the file path to the registry.
            //setPersonFilePath(file);

        } catch (Exception e) { // catches ANY exception
            Dialogs.create()
                    .title("Error")
                    .masthead("Could not load data from file:\n" + file.getPath())
                    .showException(e);
        }
    }


    public void saveDataToFile(CT_mig_9 data, File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(CT_mig_9.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            try {
                m.setProperty("com.sun.xml.internal.bind.namespacePrefixMapper", new MyNamespaceMapper());
                //m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new MyNamespaceMapper());
            } catch(PropertyException e) {
                // In case another JAXB implementation is used
            }
            // Wrapping our person data.
           // CaseListWrapper wrapper = new CaseListWrapper();
           // wrapper.setCases(case9);

            // Marshalling and saving XML to the file.
            m.marshal(data, file);
            m.marshal(data, System.out);
            // Save the file path to the registry.
            //setPersonFilePath(file);
        } catch (Exception e) { // catches ANY exception
            Dialogs.create().title("Error")
                    .masthead("Could not save data to file:\n" + file.getPath())
                    .showException(e);
        }
    }

    public ObservableList<CT_mig_9> getCase9() {
        return case9;
    }
    public void setCase9(ObservableList<CT_mig_9> case9) {
        this.case9 = case9;
    }

    private int loadSavedData() {
        CT_mig_9 tempCase = new CT_mig_9();
        Person person = new Person();
        person.setFirstNameLat("Alexey");
        person.setLastNameLat("Ivanov");
        tempCase.setHostOrg(getOrgHost());
        tempCase.setHostPerson(getPersonHost());

        tempCase.personDataDocument.person.setLastName("Норикулов");
        tempCase.personDataDocument.person.setFirstName("Мухамед");
        tempCase.personDataDocument.person.setMiddleName("Курбонович");
        tempCase.personDataDocument.person.setLastNameLat("Norikulov");
        tempCase.personDataDocument.person.setFirstNameLat("Mukhamed");
        tempCase.personDataDocument.person.setMiddleNameLat("Kurbonovich");
        tempCase.personDataDocument.person.setBirthDate("12.04.1970");
        tempCase.personDataDocument.person.gender.setElement("М");
        tempCase.personDataDocument.person.citizenship.setElement("TJK");
        tempCase.personDataDocument.person.birthPlace.setCountry(new Dict("Country", "TJK", ""));
        tempCase.personDataDocument.document.type.setElement("103008");
        tempCase.personDataDocument.document.series = new SimpleStringProperty("Т");
        tempCase.personDataDocument.document.number = new SimpleStringProperty("255000");
        tempCase.personDataDocument.document.issued = new SimpleStringProperty("04.04.2007");
        tempCase.personDataDocument.document.valid_to = new SimpleStringProperty("04.04.2012");
        tempCase.setEntrancePurpose(new Dict("VisitPurpose", "139348", ""));
        tempCase.setProfession(new Dict("Profession", "128114", ""));

        case9.add(tempCase);
        tempCase = null;

        return 1;
    }
    private int loadHostData() {
        orgHost = new Organization("1","1234567890","ООО Принимающая сторона","адресный-uid-из-фиас","12345");
        personHost = new Person("uid_person_1", "Владимир", "Хозяин");
        return 0;
    }
}