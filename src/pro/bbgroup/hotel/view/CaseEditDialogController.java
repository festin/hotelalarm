package pro.bbgroup.hotel.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pro.bbgroup.hotel.MainApp;
import pro.bbgroup.hotel.model.*;

import java.io.File;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Dialog to edit details of a person.
 *
 * @author Marco Jakob
 */
public class CaseEditDialogController {
    @FXML
    public TextField txtLastNameLat;
    @FXML
    public TextField txtFirstNameLat;
    @FXML
    public TextField txtFirstName;
    @FXML
    public TextField txtLastName;
    @FXML
    public TextField txtStayPeriodFrom;
    @FXML
    public TextField txtStayPeriodTo;
    public ChoiceBox<KeyValuePair> lstConfirmingDocType;
    public Button btnExport;
    public ChoiceBox lstGender;
    public ChoiceBox lstVisitPurpose;
    public DatePicker dateBirthDate;
    public TextField txtCitizenship;
    public ChoiceBox<KeyValuePair> lstPersonalDocType;
    public TextField txtBirthPlaceCountry;
    public TextField txtBirthPlace4;
    public TextField txtBirthPlace2;
    public TextField txtBirthPlace1;
    public TextField txtBirthPlace3;
    public TextField txtPersonalDocNumber;
    public TextField txtPersonalDocSeries;
    public DatePicker datePersonalDocIssueDate;
    public DatePicker txtConfirmingDocIssueDate;
    public TextField txtMiddleNameLat;
    public TextField txtMiddleName;

    private Stage dialogStage;
    private CT_mig_9 caseLocal;
    private boolean okClicked = false;

    private static PreparedStatement prep;
    private MainApp mainApp;

    @FXML
    private void initialize() {
        //TODO Заполнить списки информацией из справочников.
        // Загрузка сделана "изобретенным" методом
        ListConfirmingDocTypes listConfirmingDocTypes = new ListConfirmingDocTypes();
        lstConfirmingDocType.setItems(listConfirmingDocTypes.getDocTypes());
        ListGenders listGenders = new ListGenders();
        lstGender.setItems(listGenders.getGenders());
        ListVisitPurposes listVisitPurposes = new ListVisitPurposes();
        lstVisitPurpose.setItems(listVisitPurposes.getVisitPurposes());
        ListPersonalDocTypes listPersonalDocTypes = new ListPersonalDocTypes();
        lstPersonalDocType.setItems(listPersonalDocTypes.getDocTypes());

        lstGender.getSelectionModel().selectFirst();
        lstPersonalDocType.getSelectionModel().selectFirst();
        //lstVisitPurpose.getSelectionModel().selectFirst();

        dateBirthDate.setValue(LocalDate.of(2014, 01, 01));
        datePersonalDocIssueDate.setValue(LocalDate.of(2014, 01, 01));

    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setCase(CT_mig_9 tempCase) {
        this.caseLocal = tempCase;
        
        txtFirstName.setText(tempCase.personDataDocument.person.getFirstName());
        txtLastName.setText(tempCase.personDataDocument.person.getLastName());
        txtFirstNameLat.setText(tempCase.personDataDocument.person.getFirstNameLat());
        txtLastNameLat.setText(tempCase.personDataDocument.person.getLastNameLat());
        txtMiddleName.setText(tempCase.personDataDocument.person.getMiddleName());
        txtMiddleNameLat.setText(tempCase.personDataDocument.person.getMiddleNameLat());
        dateBirthDate.setValue(DateUtil.parse(tempCase.personDataDocument.person.getBirthDate()));
        txtCitizenship.setText(tempCase.personDataDocument.person.citizenship.toString());
        txtBirthPlaceCountry.setText(tempCase.personDataDocument.person.birthPlace.getCountry().getElement());
        txtBirthPlace1.setText(tempCase.personDataDocument.person.birthPlace.getPlace());
        txtBirthPlace2.setText(tempCase.personDataDocument.person.birthPlace.getPlace2());
        txtBirthPlace3.setText(tempCase.personDataDocument.person.birthPlace.getPlace3());
        txtBirthPlace4.setText(tempCase.personDataDocument.person.birthPlace.getPlace4());
        txtCitizenship.setText(tempCase.personDataDocument.person.citizenship.getElement());
        //lstVisitPurpose.setValue(tempCase.getEntrancePurpose().getElement());
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleOk() {

        if (isInputValid()) {
            /*
            try {

                prep = c.prepareStatement("insert into Cases (last_name, first_name, last_name_lat, first_name_lat, birthdate)" +
                        " values(?,?,?,?,?);");
                prep.setString(1, txtLastName.getText());
                prep.setString(2, txtFirstName.getText());
                prep.setString(3, txtLastNameLat.getText());
                prep.setString(4, txtFirstNameLat.getText());
                prep.setString(5, txtBirthDate.getText());
                prep.execute();

            } catch (SQLException ex) {
                Logger.getLogger(CaseEditDialogController.class.getName()).log(Level.SEVERE, null, ex);
            }
        `   */
            System.out.println("New case created. TODO: ");
            handleSave(new ActionEvent(null, null));
  //          caseLocal.setPerson(new Person("random_uid", txtFirstName.getText(), txtLastName.getText()));
 //           System.out.println("Confirming document type: " + lstConfirmingDocType.getValue().getKey());


            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        /*
        if (txtFirstName.getText() == null || txtFirstName.getText().length() == 0) {

            errorMessage += "No valid first name!\n";
        }
        if (txtLastName.getText() == null || txtLastName.getText().length() == 0) {
            errorMessage += "No valid last name!\n";
        }
        if (txtBirthDate.getText() == null || txtBirthDate.getText().length() == 0) {
            errorMessage += "No valid birthday!\n";
        } else {
            if (!DateUtil.validDate(txtBirthDate.getText())) {
                errorMessage += "No valid birthday. Use the format dd.mm.yyyy!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Invalid Fields")
                    .masthead("Please correct invalid fields")
                    .message(errorMessage)
                    .showError();
            return false;
        }
         */
        return true;
    }
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    public void handleSave(ActionEvent actionEvent) {

        //if (lstConfirmingDocType.get) {
         //   caseLocal.setConfirmingDocType(lstConfirmingDocType.getValue().getKey());
        //}

        //caseLocal.person.setBirthDate(dateBirthDate.getValue().toString());
        caseLocal.setHostOrg(mainApp.getOrgHost());
        caseLocal.setHostPerson(mainApp.getPersonHost());
        caseLocal.personDataDocument.person.citizenship = new Dict("Citizenship", txtCitizenship.getText(), "");
        caseLocal.personDataDocument.document.type.setValue(lstPersonalDocType.getValue().toString());
        caseLocal.personDataDocument.document.type.setElement(lstPersonalDocType.getValue().getKey());
        caseLocal.personDataDocument.person = new Person("random_uid", txtFirstName.getText(), txtLastName.getText());

        caseLocal.personDataDocument.person.setLastNameLat(txtLastNameLat.getText());
        caseLocal.personDataDocument.person.setFirstNameLat(txtFirstNameLat.getText());
        caseLocal.personDataDocument.person.setLastName(txtLastName.getText());
        caseLocal.personDataDocument.person.setFirstName(txtFirstName.getText());
        caseLocal.personDataDocument.person.setBirthDate(dateBirthDate.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        caseLocal.personDataDocument.person.birthPlace.setCountry(new Dict("Country", txtBirthPlaceCountry.getText(), ""));
        caseLocal.personDataDocument.person.birthPlace.setPlace(txtBirthPlace1.getText());
        caseLocal.personDataDocument.person.birthPlace.setPlace2(txtBirthPlace2.getText());
        caseLocal.personDataDocument.person.birthPlace.setPlace3(txtBirthPlace3.getText());
        caseLocal.personDataDocument.person.birthPlace.setPlace4(txtBirthPlace4.getText());
        caseLocal.personDataDocument.document.series = new SimpleStringProperty(txtPersonalDocSeries.getText());
        caseLocal.personDataDocument.document.number = new SimpleStringProperty(txtPersonalDocNumber.getText());
        caseLocal.personDataDocument.document.issued = new SimpleStringProperty(datePersonalDocIssueDate.getValue().toString());

        btnExport.setDisable(false);
        System.out.println("Case saved...");
    }

    public void handleExport(ActionEvent actionEvent) {
        mainApp.saveDataToFile(caseLocal, new File("export_" + caseLocal.personDataDocument.person.getLastNameLat() + ".xml"));
    }
}