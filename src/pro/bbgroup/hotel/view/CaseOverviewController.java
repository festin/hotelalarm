package pro.bbgroup.hotel.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.controlsfx.dialog.Dialogs;
import pro.bbgroup.hotel.MainApp;
import pro.bbgroup.hotel.model.CT_mig_9;
import pro.bbgroup.hotel.model.Case;
import pro.bbgroup.hotel.model.Person;

public class CaseOverviewController {
    @FXML
    private TableView<CT_mig_9> tblCase;
    @FXML
    private TableColumn<Person, String> colFirstName;
    @FXML
    private TableColumn<Person, String> colLastName;

    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label streetLabel;
    @FXML
    private Label postalCodeLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label birthdayLabel;

    // Reference to the main application.
    private MainApp mainApp;

    public CaseOverviewController() {
    }

    @FXML
    private void initialize() {
       showCaseDetails(null);
       colFirstName.setCellValueFactory(new PropertyValueFactory("firstName"));
       colLastName.setCellValueFactory(new PropertyValueFactory("lastName"));

       tblCase.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showCaseDetails(newValue));
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        tblCase.setItems(mainApp.getCase9());
        System.out.println("Got case data from mainApp! ");
    }

    private void showCaseDetails(CT_mig_9 aCase) {
        if (aCase != null) {
            System.out.println("Case selected: " + aCase.personDataDocument.person.getLastName());
        } else {
            System.out.println("Case selected: null-pointer got!");
        }
    }

    @FXML
    private void handleDeletePerson() {
        int selectedIndex = tblCase.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            tblCase.getItems().remove(selectedIndex);
        } else {
            // Nothing selected.
            Dialogs.create()
                    .title("No Selection")
                    .masthead("No Person Selected")
                    .message("Please select a person in the table.")
                    .showWarning();
        }
    }

    @FXML
    private void handleNewCase() {
        String type = "CT_mig_9";
        CT_mig_9 tempCase;

        if (type.equals("CT_mig_9")) {
            System.out.println("Creating case of type 'CT_mig_9'");
            tempCase = new CT_mig_9();
        } else {
            System.out.println("Unknown case type!");
            tempCase = null;
            System.exit(1);
        }
        boolean okClicked = mainApp.showCaseEditDialog(tempCase);
        if (okClicked) {
            System.out.println("Adding new case");
            mainApp.getCase9().add(tempCase);
        }
    }

    public void handleEditCase(ActionEvent actionEvent) {
        CT_mig_9 selectedCase = tblCase.getSelectionModel().getSelectedItem();
        if (selectedCase != null) {
            boolean okClicked = mainApp.showCaseEditDialog(selectedCase);
            if (okClicked) {
                showCaseDetails(selectedCase);
            }

        } else {
            // Nothing selected.
            Dialogs.create()
                    .title("No Selection")
                    .masthead("No Person Selected")
                    .message("Please select a person in the table.")
                    .showWarning();
        }
    }


    /**

    @FXML
    private void handleEditCase() {
        Person selectedPerson = tblCase.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            boolean okClicked = mainApp.showCaseEditDialog(selectedPerson);
            if (okClicked) {
                showPersonDetails(selectedPerson);
            }

        } else {
            // Nothing selected.
            Dialogs.create()
                    .title("No Selection")
                    .masthead("No Person Selected")
                    .message("Please select a person in the table.")
                    .showWarning();
        }
    }
    */
}